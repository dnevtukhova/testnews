package com.dnevtukhova.testnews.PagingLibrary;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dnevtukhova.testnews.R;
import com.dnevtukhova.testnews.Retrofit.Item;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PagedAdapterNews extends PagedListAdapter<Item, PagedAdapterNews.NewsViewHolder> {

   private Context context;


    public PagedAdapterNews(Context context) {
        super(DIFF_CALLBACK);

       this.context = context;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view, viewGroup, false);


        //  PagedAdapterHistory.HistoryViewHolder pvh = new PagedAdapterHistory.HistoryViewHolder(v);
        return new PagedAdapterNews.NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder newsViewHolder, int i) {

        Item newsItem = getItem(i);
        if (newsItem != null) {
            newsViewHolder.newsName.setText(newsItem.title);

         //вопрос, откуда взять контекст для вставки картинки glide

          //   Glide.with(context)
               //    .load(newsItem.stock.iconUrl)
//                    .into(historyViewHolder.stockPhoto);
//

        }
    }


        private static DiffUtil.ItemCallback<Item> DIFF_CALLBACK =
                new DiffUtil.ItemCallback<Item>() {
                    @Override
                    public boolean areItemsTheSame(Item oldItem, Item newItem) {
                        //это под вопросом (????) т.к. в оригинале было сравнение транзакций
                        return oldItem.guid.equals(newItem.guid);
                    }

                    @Override
                    public boolean areContentsTheSame(Item oldItem, @NonNull Item newItem) {
                        return oldItem.equals(newItem);
                    }
                };


       public class NewsViewHolder extends RecyclerView.ViewHolder {

            CardView cv;
            TextView newsName;

            ImageView newsPhoto;

            NewsViewHolder(View itemView) {
                super(itemView);
                cv = itemView.findViewById(R.id.cv);
                newsName = itemView.findViewById(R.id.news_shortName);

                newsPhoto = itemView.findViewById(R.id.news_photo);

            }


    }
}
