package com.dnevtukhova.testnews.PagingLibrary;

import com.dnevtukhova.testnews.Retrofit.Channel;
import com.dnevtukhova.testnews.Retrofit.Item;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PositionalDataSource;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class NewsDataSourсe extends PositionalDataSource<Item> {

//private final Item employeeStorage;
//
//public MyPositionalDataSource(Item employeeStorage) {
//        this.employeeStorage = employeeStorage;
//        }

@Override
public void loadInitial(@NonNull LoadInitialParams params, @NonNull final LoadInitialCallback<Item> callback) {
    System.out.println( "loadInitial, requestedStartPosition = " + params.requestedStartPosition +
        ", requestedLoadSize = " + params.requestedLoadSize);

    Call<Channel> call = RetrofitClient.getInstance().getApi().getNews();
        call.enqueue(new Callback<Channel>() {
            @Override
            public void onResponse(@NonNull  Call<Channel> call, @NonNull Response<Channel> response) {
                if (response.isSuccessful()) {

                    Channel channel = response.body();
                    Log.d(TAG, "заголовок из ретрофита" + channel.title);
                   System.out.println("что-то произошло" + channel.title);

                    List<Item> result = channel.itemList;
                    System.out.println("размер массива"+result.size());
                    callback.onResult(result,0, result.size());
                } else {
//                    Toast.makeText(context,
//                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

                }

            }
            @Override
            public void onFailure(@NonNull Call<Channel> call, @NonNull Throwable t) {
                System.out.println("ПРОИЗОШЛА ОШИБКА " + t);

            }
        });
        }

@Override
public void loadRange(@NonNull LoadRangeParams params, @NonNull final LoadRangeCallback<Item> callback) {

    System.out.println( "loadRange, startPosition = " + params.startPosition + ", loadSize = " + params.loadSize);

    Call<Channel> call = RetrofitClient.getInstance().getApi().getNews();
    call.enqueue(new Callback<Channel>() {
        @Override
        public void onResponse(@NonNull  Call<Channel> call, @NonNull Response<Channel> response) {
            if (response.isSuccessful()) {

                Channel channel = response.body();
                // System.out.println("что-то произошло" + channel.nextItemId);
                List<Item> result = channel.itemList;
                callback.onResult(result);
            } else {
//                    Toast.makeText(context,
//                            "Ошибка " + response.code(), Toast.LENGTH_SHORT).show();

            }

        }
        @Override
        public void onFailure(@NonNull Call<Channel> call, @NonNull Throwable t) {
            System.out.println("ПРОИЗОШЛА ОШИБКА " + t);

        }
    });
        }

        }
