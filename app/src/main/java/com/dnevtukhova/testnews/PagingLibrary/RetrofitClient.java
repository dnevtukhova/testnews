package com.dnevtukhova.testnews.PagingLibrary;

import com.dnevtukhova.testnews.Retrofit.ServerApi;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class RetrofitClient {
    private static final String BASE_URL = "https://habr.com";
    private static RetrofitClient mInstance;
    private Retrofit retrofit;


    private RetrofitClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        System.out.println("инстанс "+ mInstance);
        return mInstance;

    }

    public ServerApi getApi() {
        return retrofit.create(ServerApi.class);
    }
}
