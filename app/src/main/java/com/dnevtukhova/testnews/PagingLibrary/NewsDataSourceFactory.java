package com.dnevtukhova.testnews.PagingLibrary;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PositionalDataSource;

import com.dnevtukhova.testnews.Retrofit.Item;

public class NewsDataSourceFactory extends DataSource.Factory {
    //creating the mutable live data
    private MutableLiveData<PositionalDataSource<Item>> itemLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource<Integer, Item> create() {
        //getting our data source object
        NewsDataSourсe itemDataSource = new NewsDataSourсe();

        //posting the datasource to get the values
        itemLiveDataSource.postValue(itemDataSource);

        //returning the datasource
        return itemDataSource;
    }

    //getter for itemlivedatasource
    public MutableLiveData<PositionalDataSource<Item>> getItemLiveDataSource() {
        return itemLiveDataSource;
    }

}
