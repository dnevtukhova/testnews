package com.dnevtukhova.testnews.PagingLibrary;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;
import android.arch.paging.PositionalDataSource;

import com.dnevtukhova.testnews.Retrofit.Item;

public class NewsViewModel extends ViewModel {

    //creating livedata for PagedList  and PagedKeyedDataSource
    public LiveData<PagedList<Item>> itemPagedList;
    public LiveData<PositionalDataSource<Item>> liveDataSource;

    //constructor
    public NewsViewModel() {


        //getting our data source factory
        NewsDataSourceFactory itemDataSourceFactory = new NewsDataSourceFactory();

        //getting the live data source from data source factory
        liveDataSource = itemDataSourceFactory.getItemLiveDataSource();

        //Getting PagedList config
        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setPageSize(10)
                        .build();

        //Building the paged list
        itemPagedList = (new LivePagedListBuilder(itemDataSourceFactory, pagedListConfig))
                .build();
    }
}
