package com.dnevtukhova.testnews.Retrofit;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "rss", strict=false)
public class Channel {


    @Path("channel")
    @Element(name="title")
    public String title;

    @Path("channel")
    @ElementList(inline = true, name="item")
    public List<Item> itemList;
}
