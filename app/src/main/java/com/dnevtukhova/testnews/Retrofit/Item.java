package com.dnevtukhova.testnews.Retrofit;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "item", strict=false)
public class Item {
    @Element(name = "title")
    public String title;

    @Element(name = "guid")
    public String guid;

    @Element(name = "link")
    public String link;

    @Element(name = "description")
    public String description;
}
