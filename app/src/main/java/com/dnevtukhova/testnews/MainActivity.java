package com.dnevtukhova.testnews;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dnevtukhova.testnews.PagingLibrary.NewsViewModel;
import com.dnevtukhova.testnews.PagingLibrary.PagedAdapterNews;
import com.dnevtukhova.testnews.Retrofit.Item;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // ContextManager.setContext(this);
        // Toolbar toolbar = findViewById(R.id.toolbar_history);

        //TextView textView = findViewById(R.id.name_page);
     //   textView.setText(R.string.name_history_transactions);

        //обратить внимание, если нужен прогресс бар, то вставить !!!!!!!!!!!!!!
       // progressBarHistory = findViewById(R.id.history_progress);

//        ImageButton buttonBackHistory = findViewById(R.id.back_button);
//        buttonBackHistory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), AccountInfoActivity.class);
//                startActivity(intent);
//            }
//        });
//        showProgress(true);
        //получаем токен из персистентного хранилища
        // PersistentStorage.init(this);
//        persistentStorage = PersistentStorage.getInstance(getApplicationContext());
//        String token = persistentStorage.getProperty(getApplicationContext().getString(R.string.tokenA));
//        System.out.println("токен "+ token );
        RecyclerView rv = findViewById(R.id.rvNewsAll);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(llm);

        //getting our ItemViewModel
        NewsViewModel historyViewModel = ViewModelProviders.of(this).get(NewsViewModel.class);

        //creating the Adapter
        final PagedAdapterNews adapter = new PagedAdapterNews(this);


        //observing the itemPagedList from view model
        // -? чет это похоже то-то левое ????????????
        historyViewModel.itemPagedList.observe(this, new Observer<PagedList<Item>>() {
            @Override
            public void onChanged(@Nullable PagedList<Item> items) {

                //in case of any changes
                //submitting the items to adapter
                //  showProgress(true);
                adapter.submitList(items);
                //showProgress(false);
            }
        });
        // showProgress(false);

        //setting the adapter

        rv.setAdapter(adapter);
       // showProgress(false);

    }
}
